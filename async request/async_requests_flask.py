import asyncio
import time
from datetime import datetime

from aiohttp_requests import requests as async_request
from random import random


async def get_async_request_data(index=1, sleep=2000):
    print(f'Run {datetime.now().timestamp()} {index}: with delay {sleep}')
    resp = await async_request.get(f'http://10.4.103.241:9999/{index}')
    print(f'Run {datetime.now().timestamp()} {index}: Received {resp.status}.')
    data = await resp.text()
    print(f'Run {datetime.now().timestamp()} {index}: Data {len(data)}.')
    return data

async def main():
    tasks = []
    for i in range(50):
        tasks.append(get_async_request_data(i, random() * 0.5))
    await asyncio.gather(*tasks)

time1 = time.time()
tasks = []
asyncio.run(main())
time2 = time.time()
print(f'Done in {time2-time1}')