import datetime

from flask import Blueprint, render_template, jsonify, request
from sqlalchemy import insert

from db import db
from models import BlogModel

base_route = Blueprint('base', __name__)

@base_route.route('/<id>')
def root(id):
    print(f'{datetime.datetime.now()} start id ={id}\n')
    insert_statement = insert(BlogModel).values(title='Lorem Ipsum 1', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit')
    insert_statement2 = insert(BlogModel).values(title='Lorem Ipsum 2', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit')
    insert_statement3 = insert(BlogModel).values(title='Lorem Ipsum 3', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit')
    print(f'{datetime.datetime.now()} create 1 id ={id}\n')
    db.session.execute(insert_statement)
    print(f'{datetime.datetime.now()} create 2 id ={id}\n')
    db.session.execute(insert_statement2)
    print(f'{datetime.datetime.now()} create 3 id ={id}\n')
    db.session.execute(insert_statement3)
    print(f'{datetime.datetime.now()} create 4 id ={id}\n')
    db.session.commit()
    print(f'{datetime.datetime.now()} get data id ={id}\n')
    blogs = BlogModel.query.all()
    response_data = []
    for item in blogs:
        response_data.append({'title': item.title, 'text': item.text})
    print(f'{datetime.datetime.now()} end id ={id}\n')
    return jsonify(response_data)