from db import db
from app import app

if __name__ == '__main__':
    db.create_all(app=app)
    app.run(host='0.0.0.0', port=9999, threaded=True)
