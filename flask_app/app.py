from flask import Flask
from config import FlaskConfig
from db import db


def create_app():
    app = Flask(__name__)
    app.config.from_object(FlaskConfig)
    db.init_app(app)

    from views import base

    app.register_blueprint(base.base_route)
    return app

app = create_app()
