import asyncio

from sanic import Sanic, json
from databases import Database

app = Sanic('First try into async')
db = Database('sqlite:///new.db?uri=true')
@app.get('/<client_id>')
async def index(request, client_id):
    print(f'Client: {client_id} start')
    insert_query = 'insert into blog_model(title, text) values(:title, :text);'
    await db.execute(insert_query, dict(title='Lorem Ipsum 1', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'))
    await db.execute(insert_query, dict(title='Lorem Ipsum 2', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'))
    await db.execute(insert_query, dict(title='Lorem Ipsum 3', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'))
    print(f'Client: {client_id} middle')
    blogs = await db.fetch_all('select title, text from blog_model;')
    response_data = []
    for item in blogs:
        response_data.append({'title': item[0], 'text': item[1]})
    print(f'Client: {client_id} end')
    return json(response_data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=10000)
